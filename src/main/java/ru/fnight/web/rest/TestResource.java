package ru.fnight.web.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import ru.fnight.service.TestService;
import ru.fnight.domain.Task;
import ru.fnight.domain.TaskList;

@RestController
@RequestMapping("/api")
public class TestResource {
    @Autowired
    TestService testService;

    @RequestMapping(value = "/list/new", method = RequestMethod.POST)
    public TaskList createNewList(@RequestBody TaskList taskList) {
        return testService.createNewList(taskList);
    }

    @RequestMapping(value = "/{listId}/task/add", method = RequestMethod.POST)
    public Task addNewTask(@RequestBody Task task, @PathVariable("listId") Long listId) {
        return testService.addNewTask(task, listId);
    }

    @RequestMapping(value = "/list/{id}", method = RequestMethod.GET)
    public TaskList getListById(@PathVariable Long id) {
        return testService.getListById(id);
    }

    @RequestMapping(value = "/task/{id}", method = RequestMethod.GET)
    public Task getTaskById(@PathVariable Long id) {
        return testService.getTaskById(id);
    }

    @RequestMapping(value="/list/all", method = RequestMethod.GET)
    public List<TaskList> getAllLists() { return testService.getAllLists(); }

    @RequestMapping(value="/task/all/{listId}", method = RequestMethod.GET)
    public List<Task> getAllTasks(@PathVariable("listId") Long listId) { return testService.getAllTasks(listId); }

    @RequestMapping(value = "/task/{id}", method = RequestMethod.DELETE)
    public void deleteTask(@PathVariable Long id) { testService.deleteTask(id);
    }

    @RequestMapping(value = "/list/{id}", method = RequestMethod.DELETE)
    public void deleteList(@PathVariable Long id) {
        testService.deleteList(id);
    }

    @RequestMapping(value = "/list/{id}", method = RequestMethod.PUT)
    public TaskList completeListById(@PathVariable Long id, @RequestBody TaskList list) {
        return testService.completeListById(id, list);
    }

    @RequestMapping(value = "/task/{id}", method = RequestMethod.PUT)
    public Task completeTaskById(@PathVariable Long id, @RequestBody Task task) {
        return testService.completeTaskById(id, task);
    }
}
