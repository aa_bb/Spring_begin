package ru.fnight.domain;

public class TaskList {
    private Long id;
    private String listName;

    public TaskList() {
    }

    public TaskList(Long id, String listName) {
        this.id = id;
        this.listName = listName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    @Override
    public String toString() {
        return String.format("{id:%s,listName:%s}", id, listName);
    }
}
