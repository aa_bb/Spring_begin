package ru.fnight.domain;

public class Task {
    private Long id;
    private String description;
    private Long duration;
    private boolean isDone;
    private Long listId;

    public Task() {
    }

    public Task(Long id, Long duration, String description) {
        this.id = id;
        this.duration = duration;
        this.description = description;
        isDone = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    @Override
    public String toString() {
        return String.format("{id:%s,description:%s,duration:%s,isDone:%s,listId:%s}", id, description, duration, isDone, listId);
    }
}
