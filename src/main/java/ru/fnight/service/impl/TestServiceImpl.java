package ru.fnight.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.fnight.service.TestService;
import ru.fnight.domain.Task;
import ru.fnight.domain.TaskList;


@Service
public class TestServiceImpl implements TestService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    DataSource dataSource;

    @Override
    public TaskList createNewList(TaskList taskList) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource)
                .withTableName("task_list").usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", taskList.getId());
        parameters.put("list_name", taskList.getListName());
        Long id = simpleJdbcInsert.executeAndReturnKey(parameters).longValue();
        return getListById(id);
    }

    @Override
    public Task addNewTask(Task task, Long listId) {
        SimpleJdbcInsert simplejdbcInsert = new SimpleJdbcInsert(dataSource).
                withTableName("task").usingGeneratedKeyColumns("id");
        task.setListId(listId);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", task.getId());
        parameters.put("description", task.getDescription());
        parameters.put("duration", task.getDuration());
        parameters.put("is_done", task.getIsDone());
        parameters.put("list_id", task.getListId());
        Long id = simplejdbcInsert.executeAndReturnKey(parameters).longValue();
        return getTaskById(id);
    }

    @Override
    public TaskList getListById(Long id) {
        List<TaskList> task_lists = jdbcTemplate.query("select id, list_name from task_list where id=?",
                new BeanPropertyRowMapper<>(TaskList.class), id);
        if (task_lists.isEmpty()) {
            throw new RuntimeException("list with id =  '" + id + "' not found");
        } else {
            return task_lists.get(0);
        }
    }

    @Override
    public Task getTaskById(Long id) {
        List<Task> task = jdbcTemplate.query("select id, description, duration, is_done, list_id from task where id=?",
                new BeanPropertyRowMapper<>(Task.class), id);
        if (task.isEmpty()) {
            throw new RuntimeException("task with id = '" + id + "' not found");
        }
        else {
            return task.get(0);
        }
    }

    @Override
    public List<TaskList> getAllLists() {
        List<TaskList> taskLists = jdbcTemplate.query("select * from task_list order by id",
                new BeanPropertyRowMapper<>(TaskList.class));
        return taskLists;
    }

    @Override
    public List<Task> getAllTasks(Long listId) {
        List<Task> tasks = jdbcTemplate.query("select * from task where list_id = ? order by id",
                new BeanPropertyRowMapper<>(Task.class), listId);
        return tasks;
    }

    @Override
    public void deleteTask(Long id) {
        jdbcTemplate.update("delete from task where id=?", id);
    }

    @Override
    public void deleteList(Long id) {
        jdbcTemplate.update("delete from task where list_id=?", id);
        jdbcTemplate.update("delete from task_list where id=?", id);
    }

    @Override
    public TaskList completeListById(Long id, TaskList list) {
        String name = list.getListName();
        if (name.endsWith(" (done)"))
            name = name.substring(0, name.length() - " (done)".length());
        else
            name += " (done)";

        jdbcTemplate.update("UPDATE task_list SET list_name=? where id=?", name, id);
        return getListById(id);
    }

    @Override
    public Task completeTaskById(Long id, Task task) {
        Boolean completeState = !task.getIsDone();
        jdbcTemplate.update("UPDATE task SET is_done=? where id=?", completeState, id);
        return getTaskById(id);
    }
}
