package ru.fnight.service;

import java.util.List;
import ru.fnight.domain.TaskList;
import ru.fnight.domain.Task;

public interface TestService {
    TaskList createNewList(TaskList taskList);
    Task addNewTask(Task task, Long listId);
    TaskList getListById(Long id);
    Task getTaskById(Long id);
    List<TaskList> getAllLists();
    List<Task> getAllTasks(Long listId);
    void deleteTask(Long id);
    void deleteList(Long id);
    TaskList completeListById(Long id, TaskList list);
    Task completeTaskById(Long id, Task task);
}
