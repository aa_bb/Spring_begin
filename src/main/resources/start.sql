create table if not exists task_list(
id serial primary key,
list_name varchar(50)
);

create table if not exists task(
id serial primary key,
description text,
duration numeric(6, 2) not null,
is_done boolean default false,
list_id bigint references task_list(id)
);

